import logging
from math import radians, tan, cos, sin
import numpy

from opengl import gl
from opengl.gl_constants import *
from opengl.glfw_constants import *

from graphics_engine.shaders import Shaders
from graphics_engine.graphics_engine import perspective, look_at
from graphics_engine.model import CubeModel


def cube_demo(key_manager):
    # Instantiate a cube.
    cube = CubeModel()

    # Set numpy print precision.
    numpy.set_printoptions(precision=2, suppress=True)

    # Clear all pixels and set them to the color blue.
    gl.clear_color(0.0, 0.0, 0.4, 0.0)

    # Enable depth test.
    gl.enable(GL_DEPTH_TEST)

    # Accept fragment if it is closer to the camera than for former one.
    gl.depth_function(GL_LESS)

    vertex_arrays = gl.generate_vertex_arrays(1)
    gl.bind_vertex_array(vertex_arrays[0])

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    logging.info('Creating shaders.')
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    vertex_shader_path = 'shaders/TransformVertexShader.vertexshader'
    fragment_shader_path = 'shaders/ColorFragmentShader.fragmentshader'

    shaders = Shaders(vertex_shader_path, fragment_shader_path)

    matrix_id = gl.get_uniform_location(shaders.program_id, 'MVP')

    projection_matrix = perspective(
        radians(45.0), # FoV in radians.
        4.0 / 3.0,     # 4:3 ratio.
        0.1,           # Lower display bound.
        100            # Upper display bound.
    )

    view_matrix = look_at(
        numpy.array([4.0, 3.0, -3.0]),
        numpy.array([0.0, 0.0, 0.0]),
        numpy.array([0.0, 1.0, 0.0])
    )

    model_matrix = numpy.identity(4)

    model_view_projection = projection_matrix * view_matrix * model_matrix

    # Create buffers.
    buffer_ids = gl.generate_buffers(2)

    # Buffer data.
    gl.buffer_data(GL_ARRAY_BUFFER, cube.verticies, buffer_ids[0])
    gl.buffer_data(GL_ARRAY_BUFFER, cube.colors, buffer_ids[1])

    while not gl.window_should_close():

        # Clear the screen.
        gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Use our shader program.
        gl.use_program(shaders.program_id)

        # Send our transformation to the currently bound shader.
        gl.uniform_matrix_4fv(
            matrix_id,
            1,
            GL_TRUE,
            model_view_projection.tolist()
        )

        gl.enable_vertex_attrib_array(0)
        gl.bind_buffer(GL_ARRAY_BUFFER, buffer_ids[0])
        gl.vertex_attrib_pointer(0, 3, GL_FLOAT, GL_FALSE, 0)

        gl.enable_vertex_attrib_array(1)
        gl.bind_buffer(GL_ARRAY_BUFFER, buffer_ids[1])
        gl.vertex_attrib_pointer(1, 3, GL_FLOAT, GL_FALSE, 0)

        gl.draw_arrays(GL_TRIANGLES, 0, 12 * 3)

        gl.disable_vertex_attrib_array(0)
        gl.disable_vertex_attrib_array(1)

        gl.swap_buffers()
        gl.poll_events()

        key_manager.handle_press()

    gl.delete_buffers(buffer_ids)
    gl.delete_program(shaders.program_id)
    gl.delete_vertex_arrays(vertex_arrays)
