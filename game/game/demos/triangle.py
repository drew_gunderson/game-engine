import logging

from graphics_engine.shaders import compile_program, compile_shader
from opengl import gl
from opengl.gl_constants import *
from opengl.glfw_constants import *


def triangle_demo(key_manager):
    gl.clear_color(0.0, 0.0, 0.4, 0.0)
    vertex_arrays = gl.generate_vertex_arrays(1)
    gl.bind_vertex_array(vertex_arrays[0])

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    logging.info('Create triangle.')
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    verticies = [
        -1.0, -1.0, 0.0,
        1.0, -1.0, 0.0,
        0.0, 1.0, 0.0,
    ]

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    logging.info('Creating shaders.')
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    filename = 'shaders/SimpleVertexShader.vertexshader'
    vertex_shader_source = open(filename).read()

    filename = 'shaders/SimpleFragmentShader.fragmentshader'
    fragment_shader_source = open(filename).read()

    program_id = compile_program(
        compile_shader(vertex_shader_source, GL_VERTEX_SHADER),
        compile_shader(fragment_shader_source, GL_FRAGMENT_SHADER)
    )

    buffer_ids = gl.generate_buffers(1)
    gl.buffer_data(verticies, buffer_ids[0])

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    logging.info('Drawing triangle.')
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    while not gl.window_should_close():
        gl.clear(GL_COLOR_BUFFER_BIT)
        gl.use_program(program_id)
        gl.enable_vertex_attrib_array(0)
        gl.bind_buffer(GL_ARRAY_BUFFER, buffer_ids[0])
        gl.vertex_attrib_pointer(0, 3, GL_FLOAT, GL_FALSE, 0)

        # Draw the triangle.
        gl.draw_arrays(GL_TRIANGLES, 0, 3)

        gl.disable_vertex_attrib_array(0)

        gl.swap_buffers()
        gl.poll_events()

        key_manager.handle_press()

    gl.delete_buffers(buffer_ids)
    gl.delete_vertex_arrays(vertex_arrays)
    gl.delete_program(program_id)
