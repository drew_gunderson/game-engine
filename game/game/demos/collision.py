import logging
from math import radians, tan, cos, sin
import numpy

from opengl import gl
from opengl.gl_constants import *
from opengl.glfw_constants import *

from graphics_engine.math import perspective, look_at
from graphics_engine.text import Text_2D
from graphics_engine.model import CubeModel

import game


class collision_demo:


    def __enter__(self):
        # Instantiate cubes.
        self.cube_one = CubeModel(numpy.array([0.0, -5.0, 0.0]), 2.0)
        self.cube_two = CubeModel(numpy.array([5.0, 0.0, 0.0]), 2.0)

        # Add cubes as game objects.
        game.game_instance.game_objects.append(self.cube_one)
        game.game_instance.game_objects.append(self.cube_two)

        # Instantiate 2D text.
        self.text = Text_2D('fonts/Roboto/Roboto-Regular.ttf')

        # Set numpy print precision.
        numpy.set_printoptions(precision=2, suppress=True)

        # Clear all pixels and set them to the color blue.
        gl.clear_color(0.0, 0.0, 0.4, 0.0)

        # Enable depth test.
        gl.enable(GL_DEPTH_TEST)

        # Accept fragment if it is closer to the camera than for former one.
        gl.depth_function(GL_LESS)

        # Create identity matrix.
        self.model_matrix = numpy.identity(4)

        return self


    def run(self):

        # Run physics calculations.
        game.game_instance.physics_engine.step()

        # Clear the screen.
        gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Compute the MVP matrix.
        game.game_instance.graphics_engine.compute_matricies_from_inputs()

        projection_matrix = game.game_instance.graphics_engine.projection_matrix
        view_matrix = game.game_instance.graphics_engine.view_matrix
        model_view_projection = projection_matrix * view_matrix * self.model_matrix

        self.cube_one.draw(model_view_projection)
        self.cube_two.draw(model_view_projection)

        time_string = '{:.2f}'.format(gl.get_time())
        self.text.draw(time_string, 10, 50, 0.20, [0.0, 0.0, 0.0, 1.0])

        position = game.game_instance.graphics_engine.camera.position
        position_string = '({:.2f}, {:.2f}, {:.2f})'.format(*position)

        self.text.draw(position_string, 200, 50, 0.2, [0.0, 0.0, 0.0, 1.0])

        gl.swap_buffers()


    def __exit__(self, exc_type, exc_value, traceback):
        pass
