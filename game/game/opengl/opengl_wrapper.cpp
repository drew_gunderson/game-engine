#include <iostream>
#include <string>
#include <vector>
#include <cstddef>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/functional.h>

namespace py = pybind11;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  OpenGL Book-keeping and Context Structure
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef struct {
  GLFWwindow* window;
  GLuint version_major;
  GLuint version_minor;
  std::vector<std::vector<char> > textures;
  std::function<void(int)> python_focus_callback;
  std::function<void(int, int, int, int)> python_key_callback;
} opengl_context;

opengl_context* context;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  OpenGL Initializer
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

int initialize(std::string window_title, GLuint width, GLuint height) {
  context = new opengl_context();
  context->version_major = 3;
  context->version_minor = 3;

  if(!glfwInit()) {
		return(-1);
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, context->version_major);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, context->version_minor);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  context->window = glfwCreateWindow(width, height, window_title.c_str(), NULL,
    NULL);

  if (context->window == NULL){
    glfwTerminate();
    return(-1);
  }

  glfwMakeContextCurrent(context->window);

  glewExperimental = true;
  if (glewInit() != GLEW_OK) {
    glfwTerminate();
    return(-1);
  }

  return(0);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  Context Calls
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

bool is_initialized() {
  if (context) {
    return true;
  }

  return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  OpenGL Wrappers
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

GLuint create_shader(GLenum shader_type) {
  return glCreateShader(shader_type);
}

void shader_source(GLuint shader_id, GLsizei count, std::string source_string) {
  const char *source = source_string.c_str();
  glShaderSource(shader_id, count, &source, NULL);
}

void compile_shader(GLuint shader_id) {
  glCompileShader(shader_id);
}

GLint get_shader_iv(GLuint shader_id, GLenum parameter_name) {
  GLint result;
  glGetShaderiv(shader_id, parameter_name, &result);
  return result;
}

GLint create_program(void) {
  return glCreateProgram();
}

void attach_shader(GLuint program_id, GLuint shader_id) {
  glAttachShader(program_id, shader_id);
}

void link_program(GLuint program_id) {
  glLinkProgram(program_id);
}

void delete_program(GLuint program_id) {
  glDeleteProgram(program_id);
}

void delete_programs(std::vector<GLuint> program_ids) {
  for (auto i : program_ids) {
    delete_program(i);
  }
}

void validate_program(GLuint program_id) {
  glValidateProgram(program_id);
}

GLuint get_program_iv(GLuint program_id, GLenum parameter_name) {
  GLint result;
  glGetProgramiv(program_id, parameter_name, &result);
  return result;
}

std::string get_program_info_log(GLuint program_id) {
  GLsizei max_length = get_program_iv(program_id, GL_INFO_LOG_LENGTH);

  std::vector<char> log_c_string(max_length + 1);
  glGetProgramInfoLog(program_id, max_length, NULL, &log_c_string[0]);
  return std::string(log_c_string.begin(), log_c_string.end());
}

std::string get_shader_info_log(GLuint shader_id) {
  GLsizei max_length = get_shader_iv(shader_id, GL_INFO_LOG_LENGTH);

  std::vector<char> log_c_string(max_length + 1);
  glGetShaderInfoLog(shader_id, max_length, NULL, &log_c_string[0]);
  return std::string(log_c_string.begin(), log_c_string.end());
}

std::string get_string(GLenum string_name) {
  return(reinterpret_cast<const char*>(glGetString(string_name)));
}

void clear_color(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) {
  glClearColor(red, green, blue, alpha);
}

std::vector<unsigned int> generate_vertex_arrays(GLsizei count) {
  std::vector<unsigned int> array_ids(count);
  glGenVertexArrays(count, &array_ids[0]);
  return array_ids;
}

std::vector<unsigned int> generate_buffers(GLsizei count) {
  std::vector<unsigned int> buffers_ids(count);
  glGenBuffers(count, &buffers_ids[0]);
  return buffers_ids;
}

void buffer_data(GLenum target, std::vector<float> verticies, GLint buffer_id,
    GLenum usage) {

  glBindBuffer(target, buffer_id);
  GLsizeiptr size = verticies.size() * sizeof(verticies[0]);
  glBufferData(target, size, &verticies[0], usage);
}

void delete_shader(GLuint shader_id) {
  glDeleteShader(shader_id);
}

void use_program(GLuint program_id) {
  glUseProgram(program_id);
}

void enable_vertex_attrib_array(GLuint index) {
  glEnableVertexAttribArray(index);
}

void disable_vertex_attrib_array(GLuint index) {
  glDisableVertexAttribArray(index);
}

void bind_buffer(GLenum target, GLuint buffer) {
  glBindBuffer(target, buffer);
}

void vertex_attrib_pointer(GLuint index, GLint size, GLenum type,
    GLboolean normalized, GLsizei stride) {

  glVertexAttribPointer(index, size, type, normalized, stride, (void*)0);
}

void draw_arrays(GLenum mode, GLint first, GLsizei count) {
  glDrawArrays(mode, first, count);
}

void bind_vertex_array(GLuint array_id) {
  glBindVertexArray(array_id);
}

void delete_buffers(std::vector<unsigned int> buffers) {
  glDeleteBuffers(buffers.size(), &buffers[0]);
}

void delete_vertex_arrays(std::vector<unsigned int> arrays) {
  glDeleteVertexArrays(arrays.size(), &arrays[0]);
}

void uniform_matrix_4fv(GLint location, GLsizei count, GLboolean transpose,
    std::vector<std::vector<GLfloat> > matrix) {

  std::vector<GLfloat> flat_matrix;

  for (auto& row : matrix) {
    for (auto& item : row) {
      flat_matrix.push_back(item);
    }
  }

  glm::mat4 mvp = glm::make_mat4(&flat_matrix[0]);

  glUniformMatrix4fv(location, count, transpose, &mvp[0][0]);
}

void enable(GLenum capability) {
  glEnable(capability);
}

void disable(GLenum capability) {
  glDisable(capability);
}

void depth_function(GLenum function) {
  glDepthFunc(function);
}

GLint get_uniform_location(GLuint program_id, std::string name) {
  return glGetUniformLocation(program_id, name.c_str());
}

void active_texture(GLenum texture) {
  glActiveTexture(texture);
}

std::vector<unsigned int> generate_textures(GLsizei count) {
  std::vector<unsigned int> texture_ids(count);
  glGenTextures(count, &texture_ids[0]);
  return texture_ids;
}

void bind_texture(GLenum target, GLuint texture_id) {
  glBindTexture(target, texture_id);
}

void uniform_1i(GLint location, GLint v0) {
  glUniform1i(location, v0);
}

GLint get_attribute_location(GLuint program_id, std::string name) {
  return glGetAttribLocation(program_id, name.c_str());
}

void texture_image_2d(GLenum target, GLint level, GLint internal_format,
    GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type,
    std::vector<unsigned char> data_buffer) {

  glTexImage2D(target, level, internal_format, width, height, border, format,
    type, &data_buffer[0]);
}

void pixel_store_i(GLenum parameter_name, GLint parameter) {
  glPixelStorei(parameter_name, parameter);
}

void texture_parameter_i(GLenum target, GLenum parameter_name,
    GLint parameter) {

  glTexParameteri(target, parameter_name, parameter);
}

void texture_sub_image_2d(GLenum target, GLint level, GLint x_offset,
      GLint y_offset, GLsizei width, GLsizei height, GLenum format,
      GLenum type, std::vector<unsigned char> data_buffer) {

  glTexSubImage2D(target, level, x_offset, y_offset, width, height, format,
    type, &data_buffer[0]);
}

float get_texture_level_parameter_fv(GLenum target, GLint level,
    GLenum parameter_name) {

  GLfloat parameter;
  glGetTexLevelParameterfv(target, level, parameter_name, &parameter);
  return parameter;
}

unsigned int get_texture_level_parameter_iv(GLenum target, GLint level,
    GLenum parameter_name) {

  GLint parameter;
  glGetTexLevelParameteriv(target, level, parameter_name, &parameter);
  return parameter;
}

unsigned int get_texture_parameter_iv(GLenum target, GLenum parameter_name) {
  GLint parameter;
  glGetTexParameteriv(target, parameter_name, &parameter);
  return parameter;
}

std::vector<unsigned char> get_texture_image(GLenum target, GLint level, GLenum format,
    GLenum type, GLsizei size) {

  // Create buffer to store texture pixels.
  std::vector<unsigned char> data_buffer(size);

  // Dump data into buffer.
  glGetTexImage(target, level, format, type, &data_buffer[0]);

  return(data_buffer);
}

void activate_texture(GLenum texture) {
  glActiveTexture(texture);
}

void blend_function(GLenum source_factor, GLenum destination_factor) {
  glBlendFunc(source_factor, destination_factor);
}

void delete_textures(std::vector<unsigned int> texture_ids) {
  glDeleteTextures(texture_ids.size(), &texture_ids[0]);
}

void compressed_texture_image_2d(GLenum target, GLint level,
    GLenum internal_format, GLsizei width, GLsizei height, GLint border,
    std::vector<char> data) {

  glCompressedTexImage2D(target, level, internal_format, width, height, border,
    data.size() * sizeof(char), &data[0]);
}

GLenum get_error() {
  return(glGetError());
}

void uniform_4fv(GLint location, GLsizei count, std::vector<GLfloat> value) {
  glUniform4fv(location, count, &value[0]);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  GLFW Wrappers
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

void terminate() {
  glfwTerminate();
}

int window_should_close() {
  return glfwWindowShouldClose(context->window);
}

void set_window_should_close(int value) {
  glfwSetWindowShouldClose(context->window, value);
}

void swap_buffers() {
  glfwSwapBuffers(context->window);
}

py::tuple get_cursor_position() {
  double x;
  double y;
  glfwGetCursorPos(context->window, &x, &y);
  return py::make_tuple(x, y);
}

py::tuple get_window_size() {
  int width;
  int height;
  glfwGetWindowSize(context->window, &width, &height);
  return(py::make_tuple(width, height));
}

double get_time() {
  return(glfwGetTime());
}

void set_cursor_position(double x_position, double y_position) {
  glfwSetCursorPos(context->window, x_position, y_position);
}

void set_input_mode(int mode, int value) {
  glfwSetInputMode(context->window, mode, value);
}

int get_window_attribute(int attribute) {
  return(glfwGetWindowAttrib(context->window, attribute));
}

void focus_callback(GLFWwindow* window, int focused) {
  context->python_focus_callback(focused);
}

void set_window_focus_callback(std::function<void(int)> callback) {
  context->python_focus_callback = callback;
  glfwSetWindowFocusCallback(context->window, focus_callback);
}

int get_key(int key) {
  return(glfwGetKey(context->window, key));
}

void key_callback(GLFWwindow* window, int key, int scancode, int action,
    int modes) {

  context->python_key_callback(key, scancode, action, modes);
}

void set_key_callback(std::function<void(int, int, int, int)> callback) {
  context->python_key_callback = callback;
  glfwSetKeyCallback(context->window, key_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  PyBind11 Bindings
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

PYBIND11_MODULE(opengl_wrapper, m) {

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Context Calls.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  m.def("is_initialized", &is_initialized);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // OpenGL Calls.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  m.def("initialize", &initialize);
  m.def("clear", &glClear);
  m.def("create_shader", &create_shader);
  m.def("shader_source", &shader_source);
  m.def("compile_shader", &compile_shader);
  m.def("get_shader_iv", &get_shader_iv);
  m.def("get_shader_info_log", &get_shader_info_log);
  m.def("create_program", &create_program);
  m.def("attach_shader", &attach_shader);
  m.def("link_program", &link_program);
  m.def("delete_program", &delete_program);
  m.def("delete_programs", &delete_programs);
  m.def("validate_program", &validate_program);
  m.def("get_program_iv", &get_program_iv);
  m.def("get_program_info_log", &get_program_info_log);
  m.def("get_string", &get_string);
  m.def("clear_color", &clear_color);
  m.def("buffer_data", &buffer_data);
  m.def("delete_shader", &delete_shader);
  m.def("use_program", &use_program);
  m.def("enable_vertex_attrib_array", &enable_vertex_attrib_array);
  m.def("disable_vertex_attrib_array", &disable_vertex_attrib_array);
  m.def("bind_buffer", &bind_buffer);
  m.def("vertex_attrib_pointer", &vertex_attrib_pointer);
  m.def("draw_arrays", &draw_arrays);
  m.def("generate_vertex_arrays", &generate_vertex_arrays);
  m.def("bind_vertex_array", &bind_vertex_array);
  m.def("generate_buffers", &generate_buffers);
  m.def("delete_buffers", &delete_buffers);
  m.def("delete_vertex_arrays", &delete_vertex_arrays);
  m.def("uniform_matrix_4fv", &uniform_matrix_4fv);
  m.def("enable", &enable);
  m.def("disable", &disable);
  m.def("depth_function", &depth_function);
  m.def("get_uniform_location", &get_uniform_location);
  m.def("active_texture", &active_texture);
  m.def("generate_textures", &generate_textures);
  m.def("bind_texture", &bind_texture);
  m.def("uniform_1i", &uniform_1i);
  m.def("get_attribute_location", &get_attribute_location);
  m.def("texture_image_2d", &texture_image_2d);
  m.def("pixel_store_i", &pixel_store_i);
  m.def("texture_parameter_i", &texture_parameter_i);
  m.def("texture_sub_image_2d", &texture_sub_image_2d);
  m.def("get_texture_image", &get_texture_image);
  m.def("get_texture_parameter_iv", &get_texture_parameter_iv);
  m.def("get_texture_level_parameter_fv", &get_texture_level_parameter_fv);
  m.def("get_texture_level_parameter_iv", &get_texture_level_parameter_iv);
  m.def("activate_texture", &activate_texture);
  m.def("blend_function", &blend_function);
  m.def("delete_textures", &delete_textures);
  m.def("compressed_texture_image_2d", &compressed_texture_image_2d);
  m.def("get_error", &get_error);
  m.def("uniform_4fv", &uniform_4fv);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // GLFW Calls.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  m.def("terminate", &terminate);
  m.def("window_should_close", &window_should_close);
  m.def("set_window_should_close", &set_window_should_close);
  m.def("swap_buffers", &swap_buffers);
  m.def("poll_events", &glfwPollEvents);
  m.def("get_cursor_position", &get_cursor_position);
  m.def("get_window_size", &get_window_size);
  m.def("get_time", &get_time);
  m.def("set_cursor_position", &set_cursor_position);
  m.def("set_input_mode", &set_input_mode);
  m.def("get_window_attribute", &get_window_attribute);
  m.def("set_window_focus_callback", &set_window_focus_callback);
  m.def("get_key", &get_key);
  m.def("set_key_callback", &set_key_callback);

}
