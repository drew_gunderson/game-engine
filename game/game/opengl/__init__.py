import logging

import opengl_wrapper
from opengl.gl_constants import *


def check_error(error_result, name):
    string = '"{}": Received a {} error.'

    if error_result == GL_NO_ERROR:
        logging.debug('"{}": No error was received.'.format(name))

    if error_result == GL_INVALID_ENUM:
        raise RuntimeError(string.format(name, 'GL_INVALID_ENUM'))

    if error_result == GL_INVALID_VALUE:
        raise RuntimeError(string.format(name, 'GL_INVALID_VALUE'))

    if error_result == GL_INVALID_OPERATION:
        raise RuntimeError(string.format(name, 'GL_INVALID_OPERATION'))

    if error_result == GL_OUT_OF_MEMORY:
        raise RuntimeError(string.format(name, 'GL_OUT_OF_MEMORY'))


class OpenGL:
    def __getattribute__(self, name):

        # Allow initialization method call if OpenGL isn't initialized.
        if not opengl_wrapper.is_initialized() and name == 'initialize':
            return(getattr(opengl_wrapper, name))

        # If OpenGL is initialized allow method calls and check for errors.
        if opengl_wrapper.is_initialized():
            result = getattr(opengl_wrapper, name)
            error_result = opengl_wrapper.get_error()
            check_error(error_result, name)
            return(result)

        string = 'You must initialize OpenGL before calling "{}".'
        raise RuntimeError(string.format(name))


gl = OpenGL()
