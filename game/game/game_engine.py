'''
.. module:: Game Engine
.. moduleauthor:: Drew Gunderson drewgun@gmail.com
'''

import threading
from time import sleep
from curses import wrapper
import logging

from audio_engine import AudioEngine
from opengl import gl
from opengl.glfw_constants import *
from opengl.gl_constants import *
from demos.keyboard_and_mouse import keyboard_and_mouse_demo
from demos.collision import collision_demo
from demos import triangle, cube
from graphics_engine.graphics_engine import GraphicsEngine
from physics_engine.physics_engine import PhysicsEngine
from input_engine.input_engine import InputEngine


class GameEngine:

    # Game engine attributes.
    time_increment = 0.2
    desired_frames_per_second = 60
    current_frames_per_second = 0
    last_frame_update = 0
    engines = []
    threads = []
    quit_event = threading.Event()
    window_size = None
    key_manager = None
    window_size = (1024, 768)


    def __init__(self):

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.info('Initializing game engine.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        logging.basicConfig(filename='game.log', level=logging.DEBUG)

        # Engines.
        self.graphics_engine = GraphicsEngine()
        self.physics_engine = PhysicsEngine()

        # Game objects in the world.
        self.game_objects = []


    def setup(self):

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.info('Initializing up audio output engine.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        self.audio_engine = AudioEngine(self.quit_event)

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.info('Initializing up input engine.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        self.input_engine = InputEngine(self.quit)


    def console(self, screen):

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.info('Running ncurses output.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        # Run until the quit thread event is received.
        while not self.quit_event.is_set():

            # Clear the terminal.
            screen.clear()

            # Define string representing camera position.
            position = graphics_engine.camera.position
            position_string = '({}, {}, {})'.format(*position)

            # Output position string and instructions to quit.
            screen.addstr(0, 0, position_string)
            screen.addstr(2, 0, 'Press "Escape" to quit.')

            # Refresh what's drawn on the screen.
            screen.refresh()

            # Sleep for a tenth of a second.
            sleep(0.1)

        # Clearn the terminal.
        screen.clear()


    def window_focus_callback(self, focused):
        if not focused:
            self.input_engine.release_mouse()
        else:
            self.input_engine.capture_mouse()


    def run_graphical(self):

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.info('Initializing OpenGL.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        gl.initialize("Game", self.window_size[0], self.window_size[1])

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.info('Setting up game engine.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        self.setup()

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.info('Getting OpenGL and GLSL version information.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        open_gl_version = gl.get_string(GL_VERSION)
        glsl_version = gl.get_string(GL_SHADING_LANGUAGE_VERSION)
        logging.info('OpenGL version: {}'.format(open_gl_version))
        logging.info('GLSL version: {}'.format(glsl_version))

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.info('Running main loop.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        gl.set_window_focus_callback(self.window_focus_callback)

        with collision_demo() as demo:
            while not gl.window_should_close():
                gl.poll_events()
                demo.run()

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.info('Terminating OpenGL.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        gl.terminate()


    def quit(self):

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.info('Quitting the game.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        gl.set_window_should_close(1)

        try:

            # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            logging.info('Stopping threads and engines.')
            # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            # Send event to stop any running threads.
            self.quit_event.set()

            # Loop through engines and send stop event to initiate clean up.
            for engine in self.engines:
                engine.stop()

        except:

            # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            logging.error('Issue stopping engines.')
            # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


    def run(self):
        #wrapper(self.console)
        self.run_graphical()
