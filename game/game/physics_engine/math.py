

def is_inside(point, box):
    is_inside_x = point[0] >= box.min_x and point[0] <= box.max_x
    is_inside_y = point[1] >= box.min_y and point[1] <= box.max_y
    is_inside_z = point[2] >= box.min_z and point[2] <= box.max_z

    return is_inside_x and is_inside_y and is_inside_z
