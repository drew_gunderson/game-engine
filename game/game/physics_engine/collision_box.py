

class CollisionBox:
    def __init__(self, position, size):
        half_size = size / 2

        self.min_x = position[0] - half_size
        self.min_y = position[1] - half_size
        self.min_z = position[2] - half_size

        self.max_x = position[0] + half_size
        self.max_y = position[1] + half_size
        self.max_z = position[2] + half_size
