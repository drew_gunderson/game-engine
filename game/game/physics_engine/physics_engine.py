""" """

from enum import Enum

from physics_engine.math import is_inside
import game


class PhysicsEvent(Enum):
    CHECK_COLLISION = 1
    CHECK_GRAVITY = 2
    CHECK_FRICTION_FORCE = 3

    KINETIC_FORCE = 4

    POSITION_CHANGE = 5


class PhysicsEngine:
    def __init__(self):
        pass
        #self.event_id = event_engine.register()


    def run(self):
        #events = event_engine.get_events(self.event_id)

        for event in events:
            if event is PhysicsEvent.CHECK_COLLISION:
                pass
            if event is PhysicsEvent.CHECK_GRAVITY:
                pass
            if event is PhysicsEvent.CHECK_KINETIC_FORCE:
                pass
            if event is PhysicsEvent.CHECK_FRICTION_FORCE:
                pass


    def handle_collision_event(self):
        """ Run collision detection algorithm.

        This could be called only when an object moves and be limited to
        checking for collision only with the neares objects?

        Bounding boxes look like the simplest solution.

        In the end a redraw event needs to be sent to the graphics engine.

        """

        # event_engine.send_event((
        #     graphics_engine.event_id,
        #     GraphicsEvent.REDRAW
        # ))

        objects = game.game_instance.game_objects
        camera = game.game_instance.graphics_engine.camera

        if camera.position[1] <= 0.1:
            camera.acceleration[1] = 0.0
            camera.position[1] = 0.0

        for game_object in objects:
            if is_inside(camera.next_position, game_object.collision_box):
                camera.collision_state = True
                return

        camera.collision_state = False


    def handle_gravity_event(self):
        """

        This could be run only when a force is excerted on an object?

        """

        objects = game.game_instance.game_objects
        camera = game.game_instance.graphics_engine.camera

        if camera.position[1] > 0.0:
            camera.acceleration[1] -= 5.8


    def step(self):
        self.handle_gravity_event()
        self.handle_collision_event()
