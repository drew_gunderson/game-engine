import logging
from threading import Thread
from pyaudio import PyAudio
import wave
import sys


class AudioEngine:
    chunk = 1024
    quit_event = None
    thread = None
    

    def __init__(self, quit_event):
        self.quit_event = quit_event


    def play(self, filename):
        try:
            audio_player = PyAudio()

            wave_file = wave.open(filename, 'rb')

            sample_width = wave_file.getsampwidth()

            stream = audio_player.open(
                format=audio_player.get_format_from_width(sample_width),
                channels=wave_file.getnchannels(),
                rate=wave_file.getframerate(),
                output=True
            )

            data = wave_file.readframes(self.chunk)

            arguements = (audio_player, data, stream, wave_file)
            self.thread = Thread(target=self.handle_playback, args=arguements)
            self.thread.start()
        except:
            logging.error('Failed to play audio file.')


    def handle_playback(self, audio_player, data, stream, wave_file):
        while data and not self.quit_event.is_set():
            stream.write(data)
            data = wave_file.readframes(self.chunk)

        stream.stop_stream()
        stream.close()
        audio_player.terminate()
        self.thread.join()
