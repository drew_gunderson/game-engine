""" Event Engine Class Script

Cause and Affect

InputEvent.FORWARD -> PhysicsEvent.KINETIC_FORCE, magnitude, direction
PhysicsEvent.POSITION_CHANGE -> GraphicsEvent.REDRAW, object

Proximity limited events like interacting with another object?
"""



class EventEngine:
    def __init__(self):
        self.events = {}


    def register(self):
        # Create a new event ID.
        event_id = len(self.events.keys())

        # Map the new event ID to a new list in the dictionary of IDs.
        self.events[event_id] = []

        # Return the event ID so that event's can be tracked.
        return event_id


    def get_events(self, event_id):
        # Return list of events that have accumulated.
        return self.events[event_id]


    def send_event(self, event, recipient):
        """ Send an event to a recipient. """

        pass
