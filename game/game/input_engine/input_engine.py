
from opengl.glfw_constants import *
from opengl.gl_constants import *
from opengl import gl
import game


class InputEngine:
    look_speed = 0.005
    move_speed = 3.0
    mouse_captured = None


    def capture_mouse(self):
        self.mouse_captured = True
        gl.set_input_mode(CURSOR, CURSOR_DISABLED)


    def release_mouse(self):
        self.mouse_captured = False
        gl.set_input_mode(CURSOR, CURSOR_NORMAL)


    def __init__(self, quit):

        self.key_manager = KeyManager()
        self.mouse_manager = MouseManager()

        #
        self.capture_mouse()

        #
        self.half_width = game.game_instance.window_size[0] / 2
        self.half_height = game.game_instance.window_size[1] / 2

        #
        gl.set_input_mode(STICKY_KEYS, GL_TRUE)

        # Set the position of the cursor to the center.
        gl.set_cursor_position(self.half_width, self.half_height)


    def get_reticle_position(self):
        if self.mouse_captured:
            self.reticle_position = gl.get_cursor_position()
            gl.set_cursor_position(self.half_width, self.half_height)
            return self.reticle_position

        return((self.half_width, self.half_height))


class MouseManager:
    move_action = None


    def __init__(self):
        pass


    def add_move_event(self, move_action):
        self.move_action = move_action


    def handle_move(self):
        cursor_position = gl.get_cursor_position()
        self.move_action(cursor_position)


class KeyManager:
    events = {}

    def __init__(self):
        gl.set_key_callback(self.key_callback)

        camera = game.game_instance.graphics_engine.camera

        self.add_press_event(KEY_ESCAPE, game.game_instance.quit)

        self.add_press_event(KEY_SPACE, camera.jump, True)
        self.add_press_event(KEY_W, camera.move_forward, True)
        self.add_press_event(KEY_A, camera.move_left, True)
        self.add_press_event(KEY_S, camera.move_backward, True)
        self.add_press_event(KEY_D, camera.move_right, True)

        self.add_release_event(KEY_W, camera.move_forward, False)
        self.add_release_event(KEY_A, camera.move_left, False)
        self.add_release_event(KEY_S, camera.move_backward, False)
        self.add_release_event(KEY_D, camera.move_right, False)


    def add_press_event(self, key, press_action, *press_arguements):
        if key not in self.events:
            self.events[key] = {}

        self.events[key]['press_action'] = press_action
        self.events[key]['press_arguements'] = press_arguements


    def add_release_event(self, key, release_action, *release_arguements):
        if key not in self.events:
            self.events[key] = {}

        self.events[key]['release_action'] = release_action
        self.events[key]['release_arguements'] = release_arguements


    def key_callback(self, key, scancode, action, modes):
        if key not in self.events:
            return

        if action == PRESS and 'press_action' in self.events[key]:
            arguements = self.events[key]['press_arguements']
            self.events[key]['press_action'](*arguements)
        elif action == RELEASE and 'release_action' in self.events[key]:
            arguements = self.events[key]['release_arguements']
            self.events[key]['release_action'](*arguements)


    def handle_move(self):
        if key in self.events:
            self.events[keys]()
