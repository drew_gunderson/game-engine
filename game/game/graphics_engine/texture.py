"""

Texture Coordinates

The best analogy for texture mapping I've read is the following; Figure 1 is a
rectangular texture, but let's pretend it is a rubber sheet. We could
stick three pins [a, b, c] into spots in the sheet. Then we could stretch them
out to a new location and secure them.

Or as in Figure 3 we could use four pins and strech the texture. You can keep
going with this concept and map 2 dimensional texures onto sphere or other
complex surfaces.


           Figure 1                     Figure 2

                      c                      c
   1     - - - - - -.-o                      o
   |    |         .  .|                    / . \
   |    |       .  . .|                  /  . . \
   v    |     .  . . +|                /  . . . +\
   |    |   .  . . + +|              /   . . . + +\
   |    | .  . . + + +|            /    . . . + + +\
   0    o- - - - - - -o           o - - - - - - - - o
        a             b           a                 b

        0 ---- u ---- 1


           Figure 3                     Figure 4

        d             c           d                c
        o- - - - - -.-o           o- - - - -.- -.- -o
        |         .  .|            \               + \
        |       .  . .|             \     .   .       \
        |     .  . . +|              \           +   + \
        |   .  . . + +|               \ .   .       +   \
        | .  . . + + +|                \       +   +    +\
        o- - - - - - -o                 o- - - - - - - - -o
        a             b                 a                 b


           Figure 5                     Figure 6

         o  o  o  o  o
        / \/ \/ \/ \/ \                   , - ~.~ - ,
        |         .  .|               , '.           ' ,
        |       .  . .|              ,           .      ,
        |     .  . . +|              ,     .            ,
        |   .  . . + +|              ,.            +    ,
        | .  . . + + +|               ',     .        ,'
        \ /\ /\ /\ /\ /                 ' - , _ _ , -
         o  o  o  o  o

"""
