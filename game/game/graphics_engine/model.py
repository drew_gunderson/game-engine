import numpy
import pymesh
import logging

from opengl import gl
from opengl.gl_constants import *

from graphics_engine.shaders import Shaders
from graphics_engine.math import translate
from physics_engine.collision_box import CollisionBox


class Model:
    scale = None
    position = None
    verticies = []
    colors = []
    mesh = None


    def __init__(self, position=numpy.array([0, 0, 0])):

        self.position = position
        self.verticies = translate(self.verticies, self.position)

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.debug('Creating and binding model vertex array.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        self.vertex_arrays = gl.generate_vertex_arrays(1)
        gl.bind_vertex_array(self.vertex_arrays[0])

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.debug('Creating model shaders and getting uniforms.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        vertex_shader_path = 'shaders/TransformVertexShader.vertexshader'
        fragment_shader_path = 'shaders/ColorFragmentShader.fragmentshader'

        self.shaders = Shaders(vertex_shader_path, fragment_shader_path)

        self.matrix_id = gl.get_uniform_location(self.shaders.program_id, 'MVP')

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.debug('Buffering model verticies and colors.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        # Create buffers.
        self.buffer_ids = gl.generate_buffers(2)

        # Buffer verticies.
        gl.buffer_data(
            GL_ARRAY_BUFFER,
            self.verticies,
            self.buffer_ids[0],
            GL_STATIC_DRAW
        )

        # Buffer colors.
        gl.buffer_data(
            GL_ARRAY_BUFFER,
            self.colors,
            self.buffer_ids[1],
            GL_STATIC_DRAW
        )


    def __del__(self):
        try:
            # Delete resources.
            gl.delete_program(self.shaders.program_id)
            gl.delete_vertex_arrays(self.vertex_arrays)
            gl.delete_buffers(self.buffer_ids)
        except:
            raise RuntimeError('Destructor failed, like due to failed initialization')


    def load(self, filename):
        self.mesh = pymesh.load_mesh(filename)


    def save(self, filename):
        pymesh.save_mesh(filename, self.mesh)


    def draw(self, model_view_projection):

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        logging.debug('Drawing model.')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        # Use our shader program.
        gl.use_program(self.shaders.program_id)

        # Send our transformation to the currently bound shader.
        gl.uniform_matrix_4fv(
            self.matrix_id,
            1,
            GL_TRUE,
            model_view_projection.tolist()
        )

        gl.enable_vertex_attrib_array(0)
        gl.bind_buffer(GL_ARRAY_BUFFER, self.buffer_ids[0])
        gl.vertex_attrib_pointer(0, 3, GL_FLOAT, GL_FALSE, 0)

        gl.enable_vertex_attrib_array(1)
        gl.bind_buffer(GL_ARRAY_BUFFER, self.buffer_ids[1])
        gl.vertex_attrib_pointer(1, 3, GL_FLOAT, GL_FALSE, 0)

        gl.draw_arrays(GL_TRIANGLES, 0, len(self.verticies))

        gl.disable_vertex_attrib_array(0)
        gl.disable_vertex_attrib_array(1)


class TriangleModel(Model):
    def __init__(self, position=None, base=1.0, height=1.0):
        super().__init__(position)

        self.verticies = [
            -1.0, -1.0, 0.0,
            1.0, -1.0, 0.0,
            0.0, 1.0, 0.0,
        ]


class SquareModel(Model):
    vertex_count = 4

    def __init__(self, position=None, length=1.0, width=1.0):
        super().__init__(position)

        half_length = length / 2
        half_width = width / 2

        self.verticies = [
            [half_width, half_width, 0],
            [-half_length, half_width, 0],
            [half_width, -half_width, 0],
            [-half_width, -half_width, 0]
        ]

        self.colors = [
            [0.583, 0.771, 0.014],
            [0.609, 0.115, 0.436],
            [0.327, 0.483, 0.844],
            [0.822, 0.569, 0.201]
        ]


class CubeModel(Model):
    def __init__(self, position, size):
        self.collision_box = CollisionBox(position, size)

        self.verticies = [
            -1.0, -1.0, -1.0,
            -1.0, -1.0,  1.0,
            -1.0,  1.0,  1.0,
             1.0,  1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0,  1.0, -1.0,
             1.0, -1.0,  1.0,
            -1.0, -1.0, -1.0,
             1.0, -1.0, -1.0,
             1.0,  1.0, -1.0,
             1.0, -1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0,  1.0,  1.0,
            -1.0,  1.0, -1.0,
             1.0, -1.0,  1.0,
            -1.0, -1.0,  1.0,
            -1.0, -1.0, -1.0,
            -1.0,  1.0,  1.0,
            -1.0, -1.0,  1.0,
             1.0, -1.0,  1.0,
             1.0,  1.0,  1.0,
             1.0, -1.0, -1.0,
             1.0,  1.0, -1.0,
             1.0, -1.0, -1.0,
             1.0,  1.0,  1.0,
             1.0, -1.0,  1.0,
             1.0,  1.0,  1.0,
             1.0,  1.0, -1.0,
            -1.0,  1.0, -1.0,
             1.0,  1.0,  1.0,
            -1.0,  1.0, -1.0,
            -1.0,  1.0,  1.0,
             1.0,  1.0,  1.0,
            -1.0,  1.0,  1.0,
             1.0, -1.0,  1.0
        ]

        self.colors = [
            0.583, 0.771, 0.014,
            0.609, 0.115, 0.436,
            0.327, 0.483, 0.844,
            0.822, 0.569, 0.201,
            0.435, 0.602, 0.223,
            0.310, 0.747, 0.185,
            0.597, 0.770, 0.761,
            0.559, 0.436, 0.730,
            0.359, 0.583, 0.152,
            0.483, 0.596, 0.789,
            0.559, 0.861, 0.639,
            0.195, 0.548, 0.859,
            0.014, 0.184, 0.576,
            0.771, 0.328, 0.970,
            0.406, 0.615, 0.116,
            0.676, 0.977, 0.133,
            0.971, 0.572, 0.833,
            0.140, 0.616, 0.489,
            0.997, 0.513, 0.064,
            0.945, 0.719, 0.592,
            0.543, 0.021, 0.978,
            0.279, 0.317, 0.505,
            0.167, 0.620, 0.077,
            0.347, 0.857, 0.137,
            0.055, 0.953, 0.042,
            0.714, 0.505, 0.345,
            0.783, 0.290, 0.734,
            0.722, 0.645, 0.174,
            0.302, 0.455, 0.848,
            0.225, 0.587, 0.040,
            0.517, 0.713, 0.338,
            0.053, 0.959, 0.120,
            0.393, 0.621, 0.362,
            0.673, 0.211, 0.457,
            0.820, 0.883, 0.371,
            0.982, 0.099, 0.879
        ]

        super().__init__(position)
