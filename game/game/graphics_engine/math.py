import numpy
from math import radians, tan, cos, sin, pi
from sklearn.preprocessing import normalize


def perspective(fovy, aspect, z_near, z_far):
    tan_half_fovy = tan(fovy / 2)

    result = numpy.zeros(shape=(4, 4))

    result[0][0] =   1 / (aspect * tan_half_fovy)
    result[1][1] =   1 / tan_half_fovy
    result[2][2] = - (z_far + z_near) / (z_far - z_near)
    result[2][3] = - (2 * z_far * z_near) / (z_far - z_near)
    result[3][2] = - 1

    return(numpy.matrix(result))


def look_at(eye, center, up):
    f = normalize([center - eye])
    u = normalize([up])
    s = normalize(numpy.cross(f, u))
    u = numpy.cross(s, f)

    result = numpy.identity(4)

    result[0][0] =   s[0][0]
    result[0][1] =   s[0][1]
    result[0][2] =   s[0][2]
    result[1][0] =   u[0][0]
    result[1][1] =   u[0][1]
    result[1][2] =   u[0][2]
    result[2][0] = - f[0][0]
    result[2][1] = - f[0][1]
    result[2][2] = - f[0][2]
    result[0][3] = - numpy.dot(s[0], eye)
    result[1][3] = - numpy.dot(u[0], eye)
    result[2][3] =   numpy.dot(f[0], eye)

    return(numpy.matrix(result))


def translate(flat_vertex_array, translation):
    vertex_count = len(flat_vertex_array) / 3
    verticies = numpy.split(numpy.array(flat_vertex_array), vertex_count)

    for vertex in verticies:
        vertex += translation

    return numpy.array([verticies]).flatten()
