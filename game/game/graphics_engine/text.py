import struct
import math
from os.path import basename, splitext
from heapq import heappush

from freetype import Face, FT_LOAD_RENDER, FT_LOAD_NO_SCALE, FT_GLYPH_BBOX_UNSCALED
from PIL import Image

from opengl import gl
from opengl.gl_constants import *
from graphics_engine.shaders import Shaders


MAX_TEXTURE_SIZE = 2048


def round_up(x, rounding_number):
    return int(math.ceil(x / rounding_number)) * rounding_number


class Text_2D:

    # A map from unicode character code to glyph details.
    glyph_data_map = {}

    # List to hold glyphs sorted from smallest to largest.
    sorted_glyphs = []

    # Texture details.
    texture_width = 0
    texture_height = 0

    # Texture directory path.
    texture_directory_path = 'textures/'


    def __init__(self, font_path):
        """

        Buffers all character glyphs in a font-face in a contiguous texture,
        stores positional data about the texture, and prepares shaders for
        drawing 2D text.

        """

        self.font_path = font_path

        # Get necissary details from glyphs to create a map of the atlas.
        self.initialize_atlas_map()

        if self.texture_width > MAX_TEXTURE_SIZE:
            raise RuntimeError('Texture width greater than MAX_TEXTURE_SIZE.')

        if self.texture_height > MAX_TEXTURE_SIZE:
            raise RuntimeError('Texture height greater than MAX_TEXTURE_SIZE.')

        if not self.texture_width:
            raise RuntimeError('Texture width is zero.')

        if not self.texture_height:
            raise RuntimeError('Texture height is zero.')

        # Initialize shader program.
        self.initialize_shaders()

        # Create the texture based on the data collected in the map
        # initialization stage.
        self.initialize_atlas_texture()

        # Initialize vertex buffer objects.
        self.buffer_ids = gl.generate_buffers(2)


    def __del__(self):
        """
        """

        # Delete the buffers.
        gl.delete_buffers(self.buffer_ids)

        # Delete the textures.
        gl.delete_textures(self.texture_ids)

        # Delete the shader program.
        gl.delete_program(self.shaders.program_id)


    def mono_to_rgba(self, mono_buffer):
        rgba_buffer = []

        for pixel_value in mono_buffer:
            rgba_buffer += [0, 0, 0, pixel_value]

        return rgba_buffer


    def initialize_atlas_map(self):
        """

        Itererates through all characters in a font-face and gathers data
        regarding various positions, sizes, and relative offsets.

        """

        # Load font file.
        font_face = Face(self.font_path)

        # Define character size.
        font_face.set_pixel_sizes(0, 200)

        # Track offset while inserting bitmaps into texture.
        x_offset = 0
        y_offset = 0

        # Track the glyph with the maximum height in each row.
        max_glyph_height = 0

        # Track the row with the maximum row width.
        max_row_width = 0

        # Track the width of each row.
        row_width = 0

        row_count = 1
        glyph_area = None

        # Loop through every character in font face.
        for character_code, glyph_index in font_face.get_chars():
            if character_code > 200:
                continue

            # Load the glyph.
            font_face.load_char(character_code, FT_LOAD_RENDER)

            # Shorten references.
            glyph = font_face.glyph
            bitmap = glyph.bitmap

            # Find the highest glyph in the row.
            if bitmap.rows > max_glyph_height:
                max_glyph_height = bitmap.rows

            # Handle when a glyph will exceed MAX_TEXTURE_SIZE.
            if bitmap.width + row_width >= MAX_TEXTURE_SIZE:
                row_count += 1

                # Add the max glyph height from current row height to the
                # overall texture height.
                self.texture_height += max_glyph_height

                # Add the current row's maximum glyph height to the y offset.
                y_offset += max_glyph_height

                # Reset the maximum height and the row width for a new row.
                max_glyph_height = 0
                row_width = 0

                # Reset the glyph x offset.
                x_offset = 0

            rgba_buffer = self.mono_to_rgba(glyph.bitmap.buffer)

            self.glyph_data_map[character_code] = {
                'x': glyph.advance.x >> 6,
                'y': glyph.advance.y >> 6,
                'width': bitmap.width,
                'rows': bitmap.rows,
                'left': glyph.bitmap_left,
                'top': glyph.bitmap_top,
                'x_offset': x_offset,
                'y_offset': y_offset,
                'buffer': rgba_buffer
            }

            # Calculate area of glyph.
            glyph_area = bitmap.width * bitmap.rows

            # Store glyph in sorting dictionary.
            heappush(self.sorted_glyphs, (glyph_index, glyph_area))

            # Include this glyph's width in the offset.
            x_offset += bitmap.width

            # Extend the total width of the row to include this glyph.
            row_width += bitmap.width

            # Check to see if the current row is the widest.
            if row_width > max_row_width:
                max_row_width = row_width

        # Add the maximum glyph height from the last row.
        self.texture_height += max_glyph_height

        # Round up the texture height to the next even number.
        self.texture_height = round_up(self.texture_height, 2)

        # Define the texture width as the widest row's width rounded up.
        self.texture_width = round_up(max_row_width, 2)


    def initialize_atlas_texture(self):
        """

        Builds up the actual linear glyph texture, placing each character glyph
        end to end.

        """

        # Activate the first texture.
        gl.active_texture(GL_TEXTURE0)

        # Generate a new texture name.
        self.texture_ids = gl.generate_textures(1)

        # Bind texture name to 2D texture target.
        gl.bind_texture(GL_TEXTURE_2D, self.texture_ids[0])

        # Set texture to use texture unit 0.
        gl.uniform_1i(self.uniform_texture_id, 0)

        #
        gl.texture_image_2d(
            GL_TEXTURE_2D,
            0,
            GL_RGBA,
            self.texture_width,
            self.texture_height,
            0,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            []
        )

        # Modify uniform within shading program.
        gl.pixel_store_i(GL_UNPACK_ALIGNMENT, 1)

        gl.texture_parameter_i(
            GL_TEXTURE_2D,
            GL_TEXTURE_WRAP_S,
            GL_CLAMP_TO_EDGE
        )

        gl.texture_parameter_i(
            GL_TEXTURE_2D,
            GL_TEXTURE_WRAP_T,
            GL_CLAMP_TO_EDGE
        )

        gl.texture_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

        gl.texture_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

        for glyph_data in self.glyph_data_map.values():

            # Define sub-region of the texture with glyph.
            gl.texture_sub_image_2d(
                GL_TEXTURE_2D,
                0,
                glyph_data['x_offset'],
                glyph_data['y_offset'],
                glyph_data['width'],
                glyph_data['rows'],
                GL_RGBA,
                GL_UNSIGNED_BYTE,
                glyph_data['buffer']
            )

        self.save_texture_to_image()


    def save_texture_to_image(self):
        image_size = (self.texture_width, self.texture_height)

        texture_data = gl.get_texture_image(
            GL_TEXTURE_2D,
            0,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            self.texture_width * self.texture_height * 4
        )

        name_string = basename(self.font_path)
        name_string = splitext(name_string)[0]
        name_string += '-texture.png'

        iterator = iter(texture_data)
        rgba_values = list(zip(iterator, iterator, iterator, iterator))

        texture_image = Image.new('RGBA', image_size)
        texture_image.putdata(rgba_values)
        texture_image.save(self.texture_directory_path + name_string, 'PNG')


    def initialize_shaders(self):
        """
        """

        vertex_arrays = gl.generate_vertex_arrays(1)
        gl.bind_vertex_array(vertex_arrays[0])

        vertex_shader_path = 'shaders/TextVertexShader.vertexshader'
        fragment_shader_path = 'shaders/TextVertexShader.fragmentshader'

        self.shaders = Shaders(vertex_shader_path, fragment_shader_path)

        self.uniform_texture_id = gl.get_uniform_location(
            self.shaders.program_id,
            'textureSampler'
        )

        self.uniform_color = gl.get_uniform_location(
            self.shaders.program_id,
            'color'
        )

        # Bind the shader program.
        gl.use_program(self.shaders.program_id)


    def draw(self, text_string, x, y, scale, color):
        """
        """

        # Set texture color
        gl.uniform_4fv(self.uniform_color, 1, color)

        # Lists to store the rectangle and the texture mapping coordinates.
        verticies = []
        texture_coordinates = []

        """

        Verticies marked by a-f form two triangles that make up the
        rectangle marking a geometry that will be textured by a glyph. I chose
        to store all glyphs in a contiguous row in the atlas texture.

        I initially thought it was a advantagous to store all of the glyph
        bitmaps in a row. This is actually and issue because OpenGL limits the
        size of the texture buffer.

        a d     e
        o o - - o          o - - o - - o - - o     ---
        |\ \    |          |     |     |     |      |
        | \ \   |          |     |     |     |      |
        |  \ \  |   --->   |  0  |  1  | ... |      |
        |   \ \ |          |     |     |     |      |
        |    \ \|          |     |     |     |      |
        o - - o o          o - - o - - o - - o   y offset
        b     c f          |     |     |     |      |
                           |  .  |     |     |      |
                           |  .  |     |     |      |
                           |  .  |     |     |      |
                           |     |     |     |      |
                           o - - o - - o - - o     ---

                           |--- x offset ----|

        Rectangles with enumerations in the center represent the bounds of each
        glyph. Keep in mind that each glyph can have an irregular height and
        width, they're not all congruent.

        """

        glyph = None
        next_x = 0
        next_y = 0

        # Iterate through each character in the string.
        for character in text_string:
            glyph_key = ord(character)

            if glyph_key in self.glyph_data_map:

                # Decode as UTF-16 so that all localities are supported.
                glyph = self.glyph_data_map[glyph_key]

            else:
                RuntimeError('Character not found.')
                break

            # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            # Defines the rectangle that will be textured with the glyph.
            # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            scaled_width = glyph['width'] * scale
            scaled_height = glyph['rows'] * scale

            #
            x_alignment = x + glyph['left'] * scale
            y_alignment = -y - glyph['top'] * scale

            upper_left  = [x_alignment + next_x               , -y_alignment                ]
            upper_right = [x_alignment + next_x + scaled_width, -y_alignment                ]
            lower_right = [x_alignment + next_x + scaled_width, -y_alignment - scaled_height]
            lower_left  = [x_alignment + next_x               , -y_alignment - scaled_height]

            verticies += upper_left
            verticies += lower_left
            verticies += upper_right

            verticies += lower_right
            verticies += upper_right
            verticies += lower_left

            next_x += glyph['x'] * scale
            next_y += glyph['y'] * scale

            # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            # Defines the coordinates of the glyph on the texture.
            # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            # Offsets of glyph from edges of texture.
            x_offset = glyph['x_offset'] / self.texture_width
            y_offset = glyph['y_offset'] / self.texture_height

            #
            width = glyph['width'] / self.texture_width
            height = glyph['rows'] / self.texture_height

            upper_left  = [x_offset        , y_offset]
            upper_right = [x_offset + width, y_offset]
            lower_left  = [x_offset        , y_offset + height]
            lower_right = [x_offset + width, y_offset + height]

            texture_coordinates += upper_left
            texture_coordinates += lower_left
            texture_coordinates += upper_right

            texture_coordinates += lower_right
            texture_coordinates += upper_right
            texture_coordinates += lower_left

        # Buffer the rectangle verticies.
        gl.buffer_data(
            GL_ARRAY_BUFFER,
            verticies,
            self.buffer_ids[0],
            GL_STATIC_DRAW
        )

        # Buffer the texture map coordinates.
        gl.buffer_data(
            GL_ARRAY_BUFFER,
            texture_coordinates,
            self.buffer_ids[1],
            GL_STATIC_DRAW
        )

        # Bind the shader program.
        gl.use_program(self.shaders.program_id)

        # Bind the texture.
        gl.activate_texture(GL_TEXTURE0)
        gl.bind_texture(GL_TEXTURE_2D, self.texture_ids[0])

        # Set texture to use texture unit 0.
        gl.uniform_1i(self.uniform_texture_id, 0)

        # Create attribute buffer for the character rectangle.
        gl.enable_vertex_attrib_array(0)
        gl.bind_buffer(GL_ARRAY_BUFFER, self.buffer_ids[0])
        gl.vertex_attrib_pointer(0, 2, GL_FLOAT, GL_FALSE, 0)

        # Create attribute buffer for the texture coordinates.
        gl.enable_vertex_attrib_array(1)
        gl.bind_buffer(GL_ARRAY_BUFFER, self.buffer_ids[1])
        gl.vertex_attrib_pointer(1, 2, GL_FLOAT, GL_FALSE, 0)

        #
        gl.enable(GL_BLEND)
        gl.blend_function(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        #
        gl.draw_arrays(GL_TRIANGLES, 0, len(verticies))
        gl.disable(GL_BLEND)

        gl.disable_vertex_attrib_array(0)
        gl.disable_vertex_attrib_array(1)
