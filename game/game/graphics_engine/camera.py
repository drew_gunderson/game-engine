from math import sin, cos, radians, pi
from copy import copy

import numpy
from sklearn.preprocessing import normalize
from opengl import gl
from opengl.glfw_constants import *
import game


class Camera:
    def __init__(self, position=numpy.array([0.0, 0.0, 0.0])):
        self.position = position
        self.speed = 3.0
        self.horizontal_angle = pi
        self.verticle_angle = 0.0
        self.direction = None
        self.right = None
        self.field_of_view = 45.0
        self.last_time = None
        self.time_delta = 0
        self.acceleration = numpy.array([0.0, 0.0, 0.0])
        self.velocity = numpy.array([0.0, 0.0, 0.0])
        self.quarter_turn = numpy.array([pi / 4.0, 0, pi / 4.0])
        self.move_forward_state = False
        self.move_backward_state = False
        self.move_left_state = False
        self.move_right_state = False
        self.jump_state = False
        self.jump_start_time = None
        self.current_time = None
        self.collision_state = False
        self.next_position = position


    def update(self):

        if not self.last_time:
            self.last_time = gl.get_time()

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        # Update camera direction.
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        x_position, y_position = game.game_instance.input_engine.get_reticle_position()

        look_speed = game.game_instance.input_engine.look_speed
        window_width = game.game_instance.window_size[0]
        window_height = game.game_instance.window_size[1]

        self.horizontal_angle += look_speed * float(window_width / 2 - x_position)
        self.verticle_angle += look_speed * float(window_height / 2 - y_position)

        self.direction = numpy.array([
            cos(self.verticle_angle) * sin(self.horizontal_angle),
            sin(self.verticle_angle),
            cos(self.verticle_angle) * cos(self.horizontal_angle)
        ])

        self.right = numpy.array([
            sin(self.horizontal_angle - pi / 2.0),
            0,
            cos(self.horizontal_angle - pi / 2.0)
        ])

        self.up = numpy.array([0, 1, 0])

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        # Update any positional movements.
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        self.update_time()
        self.update_position()


    def update_time(self):
        self.current_time = gl.get_time()
        self.time_delta = float(self.current_time - self.last_time)
        self.last_time = self.current_time


    def update_position(self):
        direction = None

        if self.move_forward_state:
            direction = copy(self.direction)
        elif self.move_backward_state:
            direction = -copy(self.direction)

        if self.move_right_state and self.move_forward_state:
            direction += self.right
        elif self.move_left_state and self.move_forward_state:
            direction -= self.right

        if self.move_right_state and self.move_backward_state:
            direction += self.right
        elif self.move_left_state and self.move_backward_state:
            direction -= self.right

        if self.move_right_state and direction is None:
            direction = self.right
        elif self.move_left_state and direction is None:
            direction = -self.right

        if self.jump_state:
            self.acceleration += numpy.array([0, 40, 0])
            self.jump_state = False

        self.position += self.time_delta * self.acceleration

        if direction is not None:
            if not self.collision_state:
                self.position += direction * self.time_delta * self.speed

            self.next_position = self.position + direction


    def move_forward(self, state):
        self.move_forward_state = state


    def move_backward(self, state):
        self.move_backward_state = state


    def move_left(self, state):
        self.move_left_state = state


    def move_right(self, state):
        self.move_right_state = state


    def jump(self, state):
        self.jump_state = state
