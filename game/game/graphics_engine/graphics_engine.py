""" Graphics Engine Class Script """

from enum import Enum
from math import radians, tan, cos, sin, pi

import numpy
from opengl import gl
from opengl.glfw_constants import *
from graphics_engine.math import perspective, look_at
from graphics_engine.camera import Camera

# from event_engine import event_engine


class GraphicsEvent(Enum):
    REDRAW = 1


class GraphicsEngine:
    last_time = None

    projection_matrix = perspective(
        radians(45.0), # FoV in radians.
        4.0 / 3.0,     # 4:3 ratio.
        0.1,           # Lower display bound.
        100            # Upper display bound.
    )

    view_matrix = look_at(
        numpy.array([4.0, 3.0, -3.0]),
        numpy.array([0.0, 0.0, 0.0]),
        numpy.array([0.0, 1.0, 0.0])
    )

    camera = Camera(numpy.array([0.0, 0.0, 20.0]))
    window_is_focused = True


#     def __init__(self):
#         self.event_id = event_engine.register()
#
#     def run(self):
#         events = event_engine.get_events(self.event_id)
#
#         for event in events:
#             if event is GraphicsEvent.REDRAW:
#                 pass


    def compute_matricies_from_inputs(self):
        self.camera.update()

        right = self.camera.right
        position = self.camera.position
        direction = self.camera.direction

        up = numpy.cross(right, direction)

        self.projection_matrix = perspective(
            radians(self.camera.field_of_view),
            4.0 / 3.0,
            0.1,
            100.0
        )

        self.view_matrix = look_at(
            position,
            position + direction,
            up
        )


    def step():
        if gl.get_window_attribute(FOCUSED) != FOCUSED:
            self.window_is_focused = False

        # Compute the MVP matrix.
        self.compute_matricies_from_inputs()
