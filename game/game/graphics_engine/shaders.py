
import logging
from opengl import opengl_wrapper as gl
from opengl.gl_constants import *


class Shaders:
    program_id = None


    def __init__(self, vertex_shader_path, fragment_shader_path):
        vertex_shader_source = open(vertex_shader_path).read()
        fragment_shader_source = open(fragment_shader_path).read()

        self.program_id = compile_program(
            compile_shader(
                vertex_shader_path,
                vertex_shader_source,
                GL_VERTEX_SHADER
            ),
            compile_shader(
                vertex_shader_path,
                fragment_shader_source,
                GL_FRAGMENT_SHADER
            )
        )


def compile_program(*shaders):
    program_id = gl.create_program()

    for shader_id in shaders:
        gl.attach_shader(program_id, shader_id)

    gl.link_program(program_id)

    gl.validate_program(program_id)
    validation = gl.get_program_iv(program_id, GL_VALIDATE_STATUS)

    if validation == GL_FALSE:
        info_log = gl.get_program_info_log(program_id)
        print(info_log)

    link_status = gl.get_program_iv(program_id, GL_LINK_STATUS)
    info_log_length = gl.get_program_iv(program_id, GL_INFO_LOG_LENGTH)

    if info_log_length > 0:
        info_log = gl.get_program_info_log(program_id)
        print(info_log)

    for shader_id in shaders:
        gl.delete_shader(shader_id)

    return program_id


def compile_shader(path, source, shader_type):
    shader_id = gl.create_shader(shader_type)

    gl.shader_source(shader_id, 1, source)
    gl.compile_shader(shader_id)

    result = gl.get_shader_iv(shader_id, GL_COMPILE_STATUS)

    gl.get_shader_info_log(shader_id)

    if not result:
        statement = 'Shader "{}" compile failure (ID: {}): {}'
        statement = statement.format(
            path,
            result,
            gl.get_shader_info_log(shader_id)
        )
        print(statement)

    return shader_id
