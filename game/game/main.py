from game import game_instance


def main():
    game_instance.run()


if __name__ == '__main__':
    main()
