from distutils.core import setup
from distutils.util import get_platform
from setuptools.config import read_configuration
from setuptools import find_packages, Extension
import sysconfig
import glob
import os
import shutil


PLATFORM = get_platform()
EXTENSIONS = []
CONFIGURATION = read_configuration('pyproject.toml')
OPENGL_SOURCE_DIRECTORY_PATH = 'game/opengl'
METAL_SOURCE_DIRECTORY_PATH = 'game/metal'
OUTPUT_FILE_NAME = 'opengl_wrapper'
PATH_TEMPLATE = '{}/{}{}'
SUFFIX = sysconfig.get_config_var('EXT_SUFFIX')

# Building Boost
# $ ./bootstrap.sh
# $ ./b2 install

# Building GLEW
# $ make
# $ sudo make install
# $ make clean

# Building GLFW
# $ cmake -S . -B build
# $ cd build && make
# $ sudo make install

# Building GLM
# $ cmake -S . -B build
# $ cd build && make
# $ sudo make install

# Building PyBind11
# $ cmake -S . -B build
# $ cd build && make
# $ sudo make install



metal_wrapper = Extension(
    name=OUTPUT_FILE_NAME,
    sources=['{}/metal_wrapper.cpp'.format(METAL_SOURCE_DIRECTORY_PATH)],
    include_dirs=[
    ],
    library_dirs=[
    ],
    extra_compile_args=['-std=c++11'],
    language='c++'
)

opengl_wrapper = Extension(
    name=OUTPUT_FILE_NAME,
    sources=['{}/opengl_wrapper.cpp'.format(OPENGL_SOURCE_DIRECTORY_PATH)],
    include_dirs=[
        '/usr/local/include'
    ],
    library_dirs=[
        '/usr/local/lib',
        '/usr/local/lib/cmake/glfw3',
        '/usr/local/lib/cmake/glm'
    ],
    extra_compile_args=['-std=c++11'],
    language='c++'
)

if 'macos' in PLATFORM:
    EXTENSIONS.append(metal_wrapper)
else:
    EXTENSIONS.append(opengl_wrapper)

setup(
    name='game',
    author='Drew Gunderson',
    ext_modules=EXTENSIONS,
    packages=find_packages(),
    setup_requires=[
        'pybind11'
    ]
)


if os.path.isdir('build'):
    BUILD_PATH = None
    BUILD_DIRECTORY = glob.glob('build/lib*')[0]

    if not SUFFIX:
        SUFFIX = '.so'

    BUILD_PATH = PATH_TEMPLATE.format(BUILD_DIRECTORY, OUTPUT_FILE_NAME, SUFFIX)

    if os.path.isfile(BUILD_PATH):
        shutil.copy(BUILD_PATH, '{}/'.format(OPENGL_SOURCE_DIRECTORY_PATH))
    else:
        print('Could not find shared object.')
