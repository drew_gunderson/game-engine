
Project Goal Definitions:

	- OpenGL Wrapper:
		- To make c++ calls as pythonic and conducive to use with decorators,
			context managers, and other python paradigms.
    - To allow for existence as a stand-alone python package and git repository.

Design Tools:

	- Python
	- GLFW
	- PyAudio
	- PortAudio

Building the opengl_wrapper:

```
> python setup.py build
```

Numpy matrix indexing (row-major):

```
[0][0] [0][1] [0][2]
[1][0] [1][1] [1][2]
[2][0] [2][1] [2][2]
[3][0] [3][1] [3][2]
```

GLM matrix indexing (column-major):

```
[0][0] [1][0] [2][0]
[0][1] [1][1] [2][1]
[0][2] [1][2] [2][2]
[0][3] [1][3] [2][3]
```

Cross-compatibility:

	- Do I have to provide shader's that are compatible with other GSLS version?

Useful References:

	- https://gamedev.stackexchange.com/questions/7736/per-frame-function-calls-versus-event-driven-messaging-in-game-design
	- https://github.com/faif/python-patterns
	- https://www.quora.com/What-is-good-and-clean-event-driven-programming-in-Python-I-am-currently-experiencing-whats-called-Callback-Hell-and-want-to-know-how-to-do-it-right
	- https://en.wikipedia.org/wiki/Collision_detection#Bounding_boxes
	- http://www.peroxide.dk/papers/collision/collision.pdf
	- https://www.khronos.org/registry/OpenGL-Refpages/gl4/
	- http://www.glfw.org/docs/latest/group__window.html#gaeea7cbc03373a41fb51cfbf9f2a5d4c6
	- https://en.wikipedia.org/wiki/UV_mapping

Character Sets:

	- https://www.joelonsoftware.com/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/

Packaging:

- http://www.py2exe.org/index.cgi/Tutorial
