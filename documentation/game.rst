game package
============

Subpackages
-----------

.. toctree::

    game.opengl_wrapper

Submodules
----------

game\.audio\_engine module
--------------------------

.. automodule:: game.audio_engine
    :members:
    :undoc-members:
    :show-inheritance:

game\.camera module
-------------------

.. automodule:: game.camera
    :members:
    :undoc-members:
    :show-inheritance:

game\.event\_manager module
---------------------------

.. automodule:: game.event_manager
    :members:
    :undoc-members:
    :show-inheritance:

game\.force module
------------------

.. automodule:: game.force
    :members:
    :undoc-members:
    :show-inheritance:

game\.game module
-----------------

.. automodule:: game.game
    :members:
    :undoc-members:
    :show-inheritance:

game\.main module
-----------------

.. automodule:: game.main
    :members:
    :undoc-members:
    :show-inheritance:

game\.model module
------------------

.. automodule:: game.model
    :members:
    :undoc-members:
    :show-inheritance:

game\.shaders module
--------------------

.. automodule:: game.shaders
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: game
    :members:
    :undoc-members:
    :show-inheritance:
